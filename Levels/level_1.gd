extends Node2D

func _on_maze_1_body_entered(_body):
	var score_keeper = get_node("/root/ScoreKeeper")
	score_keeper.deaths += 1
	call_deferred("reload_scene")

func reload_scene():
	get_tree().reload_current_scene()
