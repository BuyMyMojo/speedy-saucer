extends RigidBody2D

var move_force: int = 25

func _physics_process(_delta):
	if Input.is_action_pressed("move_left"):
		apply_impulse(Vector2(-move_force, 0))
	if Input.is_action_pressed("move_right"):
		apply_impulse(Vector2(move_force, 0))
	if Input.is_action_pressed("move_up"):
		apply_impulse(Vector2(0, -move_force))
	if Input.is_action_pressed("move_down"):
		apply_impulse(Vector2(0, move_force))
	
func _process(delta):
	if Input.is_action_just_pressed("reset"):
		get_tree().reload_current_scene()
